from flask.blueprints import Blueprint
from flask import Response, request

import json

basepath = "/superapp/neto/carrito-compras"
path_productos = "/gestion-productos/v1"

path_catalogos = "/gestion-catalogos/v1"

path_categorias = "/gestion-categorias/v1"

path_etiquetas = "/gestion-etiquetas/v1"

path_promociones = "/gestion-promociones/v1"

path_sucursales = "/gestion-sucursales/v1"

productos = None
with open("productos.json") as file:
    productos = file.read()

catalogos = None
with open("catalogos.json") as file:
    catalogos = file.read()

categorias = None
with open("categorias.json") as file:
     categorias = file.read()

etiquetas = None
with open("etiquetas.json") as file:
     etiquetas = file.read()


promosiones = None
with open("promosiones.json") as file:
     promosiones = file.read()


sucursales = None
with open("sucursales.json") as file:
     sucursales = file.read()

error_401 = None
with open("error_401.json") as file:
    error_401 = file.read()

error_400 = None
with open("error_400.json") as file:
    error_400 = file.read()

error_500 = None
with open("error_500.json") as file:
    error_500 = file.read()


blueprint_productos = Blueprint("productos", __name__)


@blueprint_productos.route(basepath+path_productos+"/productos/busquedas", methods=['POST'])
def productos_post():

    payload = request.json
    if request.headers.get("Authorization", None) != "Bearer ANyIr0jDCnvjTj54sdIaWye6GKXqXXX":
        return Response(error_401, 401, mimetype="application/json")
    if payload.get("pagina", -1) == -1:
        return Response(error_400, 400, mimetype="application/json")
    elif str(payload.get("sku", None)) == "1":
        return Response(error_500, 500, mimetype="application/json")

    return Response(productos, mimetype="application/json")


@blueprint_productos.route(basepath+path_catalogos+"/catalogos", methods=['GET'])
def catalogos_get():

    payload = request.args
    if request.headers.get("Authorization", None) != "Bearer ANyIr0jDCnvjTj54sdIaWye6GKXqXXX":
        return Response(error_401, 401, mimetype="application/json")
    if int(payload.get("pagina", 0)) == -1:
        return Response(error_400, 400, mimetype="application/json")
    elif str(payload.get("sku", None)) == "1":
        return Response(error_500, 500, mimetype="application/json")

    return Response(catalogos, mimetype="application/json")


@blueprint_productos.route(basepath+path_categorias+"/categorias", methods=['GET'])
def categorias_get():

    payload = request.args
    if request.headers.get("Authorization", None) != "Bearer ANyIr0jDCnvjTj54sdIaWye6GKXqXXX":
        return Response(error_401, 401, mimetype="application/json")
    if int(payload.get("pagina", 0)) == -1:
        return Response(error_400, 400, mimetype="application/json")
    elif str(payload.get("sku", None)) == "1":
        return Response(error_500, 500, mimetype="application/json")

    return Response(categorias, mimetype="application/json")


@blueprint_productos.route(basepath+path_etiquetas+"/etiquetas", methods=['GET'])
def etiquetas_get():

    payload = request.args
    if request.headers.get("Authorization", None) != "Bearer ANyIr0jDCnvjTj54sdIaWye6GKXqXXX":
        return Response(error_401, 401, mimetype="application/json")
    if int(payload.get("pagina", 0)) == -1:
        return Response(error_400, 400, mimetype="application/json")
    elif str(payload.get("sku", None)) == "1":
        return Response(error_500, 500, mimetype="application/json")

    return Response(etiquetas, mimetype="application/json")

@blueprint_productos.route(basepath+path_promociones+"/promociones", methods=['GET'])
def promosiones_get():

    payload = request.args
    if request.headers.get("Authorization", None) != "Bearer ANyIr0jDCnvjTj54sdIaWye6GKXqXXX":
        return Response(error_401, 401, mimetype="application/json")
    if int(payload.get("pagina", 0)) == -1:
        return Response(error_400, 400, mimetype="application/json")
    elif str(payload.get("sku", None)) == "1":
        return Response(error_500, 500, mimetype="application/json")

    return Response(promosiones, mimetype="application/json")


@blueprint_productos.route(basepath+path_sucursales+"/sucursales", methods=['GET'])
def sucursales_get():

    payload = request.args
    if request.headers.get("Authorization", None) != "Bearer ANyIr0jDCnvjTj54sdIaWye6GKXqXXX":
        return Response(error_401, 401, mimetype="application/json")
    if int(payload.get("pagina", 0)) == -1:
        return Response(error_400, 400, mimetype="application/json")
    elif str(payload.get("sku", None)) == "1":
        return Response(error_500, 500, mimetype="application/json")

    return Response(sucursales, mimetype="application/json")

