from flask import Flask, Response, request
import json

app = Flask(__name__)

fake_response = None
with open("oauth.json", 'r') as file:
    fake_response = json.loads(file.read())

error_401 = None
with open("error_401.json", 'r') as file:
    error_401 = json.loads(file.read())


@app.route("/oauth2/v1/token", methods=["GET", "POST"])
def saludo():
    print(request)
    authorization = request.authorization
    if authorization:
	    if authorization.get('username', None) == "client_id_MOCK" and authorization.get('password', None) == "password_mock":
        	return Response(json.dumps(fake_response), mimetype="application/json")

    return Response(json.dumps(error_401), mimetype="application/json")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=60000)
