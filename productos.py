import flask
from productosController import blueprint_productos
from flask import Response

app = flask.Flask(__name__)


app.register_blueprint(blueprint_productos)

error_404 = None
with open("error_404.json") as file:
    error_404 = file.read()


@app.errorhandler(404)
def page_not_found(error):
    return Response(error_404, 404, mimetype="application/json")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=60001)
